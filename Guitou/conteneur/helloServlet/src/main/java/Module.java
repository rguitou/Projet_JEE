import com.google.inject.servlet.ServletModule;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

public class Module extends ServletModule {
    /*Logger*/
    private static final Logger logger =
            LoggerFactory.getLogger(Module.class);

    @Override
    protected void configureServlets() {
        super.configureServlets();
        serve("/room").with(RoomServlet.class);
        logger.info("WebModule configureServlets ended.");
    }
}