/**
 * Created by rguitou on 27/02/18.
 */
public class Room {

    public Room(String name, int occupation, int total){
        this.name = name;
        this.occupation = occupation;
        this.total = total;
    }

    public String name;

    public int occupation;

    public int total;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
