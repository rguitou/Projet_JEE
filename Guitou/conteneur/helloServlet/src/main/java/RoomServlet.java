import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rguitou on 27/02/18.
 */
@WebServlet(name = "room", urlPatterns = {"/room"})
@ServletSecurity(@HttpConstraint(rolesAllowed = "adminApp"))
public class RoomServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate =
                    freemarkerConfiguration.getTemplate("templates/listroom.ftl");
        } catch (IOException e) {
            System.out.println("Unable to process request, error during freemarker template retrieval."); }
        Map<String, Object> root = new HashMap<String, Object>();
        ArrayList<Room> rooms = new ArrayList<Room>();
        Room one = new Room("Tour de Pise", 0, 5000);
        Room two = new Room("Arc de triomphe", 5962, 12000);
        rooms.add(one);
        rooms.add(two);
        root.put("title", "Room's servlet");
        root.put("fakeRooms", rooms);
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        }
        catch (TemplateException e) { e.printStackTrace(); }
// set mime type
        response.setContentType("text/html");
    }
}
