package fr.iut.rm.control;

import com.google.inject.Inject;
import com.google.inject.persist.UnitOfWork;
import fr.iut.rm.persistence.AccessEvent;
import fr.iut.rm.persistence.Flux;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by cjohnen on 02/02/17.
 */
public class ControlRoom {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ControlRoom.class);

    /**
     * Unit of work is used to drive DB Connection
     */
    @Inject
    UnitOfWork unitOfWork;

    /**
     * Data Access Object for rooms
     */
    @Inject
    RoomDao roomDao;

    private ArrayList<AccessEvent> diary = new ArrayList<AccessEvent>();

     /*
     * * Displays all the rooms content in DB
     */
    public void showRooms() {
        unitOfWork.begin();

        List<Room> rooms = roomDao.findAll();
        if (rooms.isEmpty()) {
            System.out.println("No room");
        } else {
            System.out.println("Rooms :");
            System.out.println("--------");
            for (Room room : rooms) {
                System.out.println(String.format("   [%d], name '%s', description '%s' ", room.getId(), room.getName(), room.getDescription()));
            }
        }

        unitOfWork.end();
    }

    /**
     * Creates a room in DB
     *
     * @param name the name of the room
     */
    public void deleteRoom(final String name){
        unitOfWork.begin();

        // TODO check unicity

        Room room = roomDao.findByName(name);
        System.out.println(String.format(" Suppression de  name '%s', description '%s' ",  room.getName(), room.getDescription()));

        roomDao.removeRoom(name);
        unitOfWork.end();
    }

    public void createRoom(final String name , final String desc) {
        unitOfWork.begin();

        // TODO check unicity

        Room room = new Room();
        room.setName(name);
        room.setDescription(desc);

        System.out.println(String.format(" Insertion de  name '%s', description '%s' ",  room.getName(), room.getDescription()));

        roomDao.saveOrUpdate(room);
        unitOfWork.end();
    }

    public void comeIn(final String roomName, final String personName){
        Date d = new Date();
        AccessEvent entry = new AccessEvent(personName, Flux.ENTREE, d, roomDao.findByName(roomName));
        diary.add(entry);
        System.out.println(String.format("'%s' est entrée dans la chambre '%s' au temps '%s'", personName, roomName, d.getTime() ));
    }

    public void comeOut(final String roomName, final String personName){
        Date d = new Date();
        AccessEvent exit = new AccessEvent(personName, Flux.SORTIE, d, roomDao.findByName(roomName));
        diary.add(exit);
        System.out.println(String.format("'%s' est sorti de la chambre '%s' au temps '%s'", personName, roomName, d.getTime() ));
    }

    public void eventList(){
        for(AccessEvent a : diary){
            if(a.getType() == Flux.SORTIE)
                System.out.println(a.getName()+" est sorti de la chambre "+a.getConcerned().getName()+" le "+a.getHour().toString());
            if(a.getType() == Flux.ENTREE)
                System.out.println(a.getName()+" est entré dans la chambre "+a.getConcerned().getName()+" le "+a.getHour().toString());
        }
    }

}
