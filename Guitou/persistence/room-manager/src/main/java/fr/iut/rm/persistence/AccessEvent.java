package fr.iut.rm.persistence;

import fr.iut.rm.persistence.domain.Room;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

public class AccessEvent {

    public AccessEvent(){};

    public AccessEvent(String name, Flux type, Date hour, Room concerned){
        this.name = name;
        this.type = type;
        this.hour = hour;
        this.concerned = concerned;
    }

    @Id
    @GeneratedValue
    private long id;

    private String name;

    private Flux type;

    private Date hour;

    @ManyToOne
    private Room concerned;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Flux getType() {
        return type;
    }

    public Date getHour() {
        return hour;
    }

    public Room getConcerned() {
        return concerned;
    }
}
