package fr.iut;
import com.google.inject.name.Names;
import com.google.inject.AbstractModule;
/**
 * Created by rguitou on 09/02/18.
 */
public class MainModuleExperimental extends AbstractModule {
    @Override
    protected final void configure() {
        bind(Caddy.class);
        bind(Club.class).annotatedWith(Names.named("Putter")).to(PutterExperimental.class);
        bind(Club.class).annotatedWith(Names.named("Wood")).to(WoodExperimental.class);
    }
}
