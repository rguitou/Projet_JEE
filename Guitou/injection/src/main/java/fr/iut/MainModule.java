package fr.iut;

import com.google.inject.name.Names;
import com.google.inject.AbstractModule;

/**
 * The Guice driven Container
 */
public class MainModule extends AbstractModule {
    @Override
    protected final void configure() {
        bind(Caddy.class);
        bind(Club.class).annotatedWith(Names.named("Putter")).to(Putter.class);
        bind(Club.class).annotatedWith(Names.named("Wood")).to(Wood.class);
    }
}