package fr.iut;

import fr.iut.bean.*;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;


public class AppTest extends TestCase
{
    Person p;
    Person p2 = new Person("Jean", "Jean", true);
    Person p3 = new Person("Prof", "Chen", false);
    Person p4 = new Person("Light", "Yagami", true);
    Person p5 = new Person("Indiana", "Jones", false);
    Date j = new Date(2026115);
    Document doc = new Document("", j, p);

    ArrayList<Person> teachers = new ArrayList<Person>();
    ArrayList<Person> students = new ArrayList<Person>();
    ArrayList<Person> customers = new ArrayList<Person>();



    private static Validator validator;
    private Set<ConstraintViolation<Person>> violations;
    private Set<ConstraintViolation<Project>> violationsP;
    private Set<ConstraintViolation<Document>> violationsD;

    @BeforeClass
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void tests(){
        setUp();
        testPerson();
        testProject();
        testDoc();
    }




    @Test
    public void testPerson() {
        p2.setEmail("21");
        p2.setLogin("Jean0");
        violations = validator.validate(p2);
        assertEquals(2, violations.size());
    }

    @Test
    public void testProject(){
        teachers.add(p);
        teachers.add(p3);
        students.add(p2);
        students.add(p4);
        customers.add(p5);
        Project pro = new Project("un bon projet",teachers, students, customers);
        violationsP = validator.validate(pro);
        assertEquals(1, violationsP.size());
    }

    @Test
    public void testDoc(){
        violationsD = validator.validate(doc);
        assertEquals(1, violationsD.size());
    }

}
