package fr.iut.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by rguitou on 09/02/18.
 */
public class LoginValidator implements ConstraintValidator<Login, String> {

    private final static int BORNE_SUP = 7;

    private final static int BORNE_INF = 1;

    @Override
    public void initialize(final Login login) {

    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
        return value.length() > BORNE_INF && value.length() < BORNE_SUP && value.matches("[a-zA-Z]+");
    }
}
