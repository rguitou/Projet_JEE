package fr.iut.bean;

import javax.validation.constraints.Past;
import java.util.Date;

public class DueDocument extends Document {

    DueDocument(String title, Date creationDate, Person creator, Date dueDate){
        super(title, creationDate, creator);
        this.dueDate = dueDate;
    }

    @Past
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

}