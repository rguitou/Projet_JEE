package fr.iut.bean;

import fr.iut.validation.Login;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

public class Person {

    public Person(){};

    public Person(String firstName, String lastName, boolean isStudent ){
        this.firstName = firstName;
        this.lastName = lastName;
        this.isStudent = isStudent;
    }

    @Email
    private String email;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @Login
    private String login;

    @NotNull
    private boolean isStudent;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }
}