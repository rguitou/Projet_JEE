package fr.iut.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

/**
 * Created by rguitou on 09/02/18.
 */
public class Document {

    public Document(String title, Date creationDate, Person creator){
        this.title = title;
        this.creationDate = creationDate;
        this.creator = creator;
    }

    @NotNull
    private String title;

    private String content;

    @NotNull
    @Past
    private Date creationDate;


    @Past
    private Date lastModification;

    @NotNull
    private Person creator;

    private Person lastModifier;

    public Date getLastModification() {
        return lastModification;
    }

    public Person getCreator() {
        return creator;
    }

    public Person getLastModifier() {
        return lastModifier;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setLastModification(Date lastModification) {
        this.lastModification = lastModification;
    }

    public void setCreator(Person creator) {
        this.creator = creator;
    }

    public void setLastModifier(Person lastModifier) {
        this.lastModifier = lastModifier;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
