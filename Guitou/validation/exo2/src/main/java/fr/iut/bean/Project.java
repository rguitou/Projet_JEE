package fr.iut.bean;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

public class Project {

    public Project(String name, ArrayList<Person> teachers, ArrayList<Person> students, ArrayList<Person> customers){
        this.name = name;
        this.teachers = teachers;
        this.students = students;
        this.customers = customers;
    }

    private String description;

    @NotNull
    private String name;

    private String urlRepository;

    @NotNull
    private ArrayList<Person> teachers;

    @NotNull
    private ArrayList<Person> students;

    @NotNull
    private ArrayList<Person> customers;

    @NotNull
    private ArrayList<Document> mesDocuments;

    public void setMesDocuments(ArrayList<Document> mesDocuments) {
        this.mesDocuments = mesDocuments;
    }

    public ArrayList<Document> getMesDocuments() {
        return mesDocuments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlRepository() {
        return urlRepository;
    }

    public void setUrlRepository(String urlRepository) {
        this.urlRepository = urlRepository;
    }

    public ArrayList<Person> getTeachers() {
        return teachers;
    }

    public void setTeachers(ArrayList<Person> teachers) {
        this.teachers = teachers;
    }

    public ArrayList<Person> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Person> students) {
        this.students = students;
    }

    public ArrayList<Person> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Person> customers) {
        this.customers = customers;
    }
}