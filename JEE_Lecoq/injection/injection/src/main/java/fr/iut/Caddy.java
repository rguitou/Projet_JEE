package fr.iut;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * Created by tlecoq on 09/02/18.
 */
public class Caddy {

    @Inject
    @Named("Putter")
    private Club putter;

    @Inject
    @Named("Wood")
    private Club wood;

    public Caddy() { }

    /**
     * @param conditions
     * @return the appropriate club according to conditions
     */
    public Club getClub(final Conditions conditions) {
        switch (conditions) {
            case GREEN:
                return putter;
            case FAIRWAY:
                return wood;
            default:
                return putter;
        }
    }
}
