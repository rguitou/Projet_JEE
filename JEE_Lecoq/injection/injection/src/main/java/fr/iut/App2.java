package fr.iut;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Created by tlecoq on 09/02/18.
 */
public class App2 {
    public static void main(String[] args) {
        Injector injector2 = Guice.createInjector(new MainModuleExperimental());
        Caddy caddy = injector2.getInstance(Caddy.class);
        Player player2 = new Player("Patrick", caddy);
        System.out.println(player2);
        player2.play(0.8, Math.PI / 2, Conditions.FAIRWAY);
        System.out.println(player2);
        player2.play(0.1, Math.PI / 2, Conditions.GREEN);
        System.out.println(player2);
    }
}
