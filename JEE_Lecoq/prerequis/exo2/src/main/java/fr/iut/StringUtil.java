package fr.iut;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by tlecoq on 30/01/18.
 */

public class StringUtil {
    public static String prettyCurrencyPrint(final double amount, final Locale locale) {
        String montant = NumberFormat.getCurrencyInstance(locale).format(amount);
        System.out.println(montant);
        return montant;
    }
}
