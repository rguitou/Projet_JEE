package fr.iut;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.util.Locale;

/**
 * Created by tlecoq on 30/01/18.
 */
public class StringUtilTest {

    @Test
    public void test() {
        StringUtil test = new StringUtil();
        String s = test.prettyCurrencyPrint(21050, Locale.FRANCE);
        assertEquals(s,"21 050,00 €");

        StringUtil test2 = new StringUtil();
        String s1 = test2.prettyCurrencyPrint(-21050, Locale.FRANCE);
        assertEquals(s1,"-21 050,00 €");

        StringUtil test3 = new StringUtil();
        String s2 = test3.prettyCurrencyPrint(-21050.0454150, Locale.FRANCE);
        assertEquals(s2,"-21 050,05 €");

        StringUtil test4 = new StringUtil();
        String s3 = test4.prettyCurrencyPrint(-21050, Locale.FRANCE);
        assertEquals(s3,"-21 050,00 €");


    }
}
