/**
 * Created by tlecoq on 27/02/18.
 */
public class Room {

    public Room(String nom, int occupation, int capacite)
    {
        this.name = nom;
        this.occupation = occupation;
        this.capacity = capacite;
    }


    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int occupation;

    public int capacity;

}
