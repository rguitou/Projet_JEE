import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tlecoq on 27/02/18.
 */

@WebServlet(name = "list", urlPatterns = {"/list"})
public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/listRoom.ftl");
        } catch (IOException e) {
            System.out.println("Unable to process request,error during freemarker template retrieval.");
        }

        ArrayList<Room> rooms = new ArrayList<Room>();

        Room r1 = new Room("r1", 0, 1000);
        Room r2 = new Room("r2", 10, 5000);
        Room r3 = new Room("r3", 5632, 5632);

        rooms.add(r1);
        rooms.add(r2);
        rooms.add(r3);

        Map<String, Object> root = new HashMap<String, Object>();
        // navigation data and links
        root.put("title", "Liste des salles");
        root.put("fakeRooms", rooms);
        //root.put("now", SimpleDateFormat.getDateTimeInstance().format(new Date()));
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        }
        catch (TemplateException e) {
            e.printStackTrace();
        }
        // set mime type
        response.setContentType("text/html");
    }
}
