package fr.iut.bean;

import java.util.Date;

/**
 * Created by tlecoq on 09/02/18.
 */
public class Document {
    private String title;

    private String content;

    private Date creationDate;

    private Date lastModification;

    private Person creator;

    private Person lastModifier;
}
