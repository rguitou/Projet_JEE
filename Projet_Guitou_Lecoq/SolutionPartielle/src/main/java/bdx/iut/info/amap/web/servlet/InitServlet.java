package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Simple servlet which fills the database with dummy stuffs
 */
@Singleton
public class InitServlet extends HttpServlet {

    private boolean initialized = false;

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(InitServlet.class);

    /**
     * Injected contract DAO
     */
    @Inject
    private ContractDao contractDao;


    /**
     * Injected subscriber DAO
     */
    @Inject
    private SubscriberDao subscriberDao;

    /**
     * HTTP GET access
     *
     * @param req  use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException      by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        if(!initialized){
            BufferedReader reader = req.getReader();

            resp.setContentType("text/html; charset=UTF-8");
            PrintWriter out = new PrintWriter(new OutputStreamWriter(resp.getOutputStream(), "UTF8"), true);


            try {
                ArrayList<Contract> contracts = new ArrayList<Contract>();


                Contract legumes = new Contract();
                legumes.setProducer("Mr Poireau");
                legumes.setTitle("[légume] Contrat légumes frais");
                legumes.setAmount(5 * 9);


                Contract pain = new Contract();
                pain.setProducer("Mme Brioche");
                pain.setTitle("[divers] Contrat pain");
                pain.setAmount(5);


                Subscriber toto = new Subscriber();
                toto.setId("amapuser1");
                toto.setName("toto");
                toto.setSurname("titi");
                toto.setMail("toto@mail.com");
                toto.setAddress("15 rue du bidule 33000 Bordeaux");


                contractDao.create(legumes);
                contractDao.create(pain);


                subscriberDao.create(toto);
                toto.subscribe(legumes);
                toto.subscribe(pain);
                toto = subscriberDao.update(toto);


                Subscriber tutu = new Subscriber();
                tutu.setId("amapuser2");
                tutu.setName("hello");
                tutu.setSurname("world");
                tutu.setMail("tutu@mail.com");
                tutu.setAddress("18 rue du machin 75000 Paris");
                subscriberDao.create(tutu);
                /*

                for (Contract contract : contractDao.findAll()) {
                    out.print("<b>" + contract.getTitle() + "</b> :  " + contract.getAmount() + "€ par " + contract.getProducer());
                }
                out.print("</ul>");


                out.print("<h1>Listing des adhérents</h1>");
                for (Subscriber subscriber : subscriberDao.findAll()) {
                    out.println(subscriber.toString());
                }


                out.print("done");
                */

                initialized = true;

            } catch (org.hibernate.exception.ConstraintViolationException e) {

                out.print("<b>ERROR - I guess it is because everythn is already in memory</b>");
                out.print(e);
            }
        }
        resp.sendRedirect("main");
    }
}
