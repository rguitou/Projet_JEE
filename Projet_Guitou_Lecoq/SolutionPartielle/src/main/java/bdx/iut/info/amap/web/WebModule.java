package bdx.iut.info.amap.web;

import bdx.iut.info.amap.web.servlet.*;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fred on 08/03/15.
 */
public class WebModule extends ServletModule {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WebModule.class);

    @Override
    protected final void configureServlets() {
        logger.info("WebModule configureServlets started...");
        super.configureServlets();

        install(new JpaPersistModule("amap-manager"));

        logger.info("   install servlet filter");
        filter("/*").through(PersistFilter.class);

        logger.info("Install filters.");

        serve("/home").with(HomeServlet.class);
        serve("/admin/init").with(InitServlet.class);
        serve("/admin/main").with(AdminManageServlet.class);
        serve("/admin/contract").with(ContractManageServlet.class);
        serve("/visitor/init").with(InitServlet.class);
        serve("/visitor/main").with(VisitorServlet.class);
        serve("/subscriber/init").with(InitServlet.class);
        serve("/subscriber/main").with(AuthSubscriberServlet.class);
        serve("/subscriber/subsheet").with(SubscriberManageServlet.class);

        logger.info("WebModule configureServlets ended.");
    }
}
