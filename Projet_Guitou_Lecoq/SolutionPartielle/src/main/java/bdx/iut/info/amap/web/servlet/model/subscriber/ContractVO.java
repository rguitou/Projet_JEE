package bdx.iut.info.amap.web.servlet.model.subscriber;

/**
 * Defines a contract for the subscriber view layer (Contract Visual Object)
 */
public class ContractVO {
    /**
     * Contract id
     */
    private long id;

    /**
     * Contract title
     */
    private String title;

    /**
     * Producer associated to the contract
     */
    private String producer;

    /**
     * Amount..
     */
    private double amount;

    /**
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets new id
     *
     * @param id new value of id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets new title
     *
     * @param title new value of title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return producer
     */
    public String getProducer() {
        return producer;
    }

    /**
     * Sets new producer
     *
     * @param producer new value of producer.
     */
    public void setProducer(String producer) {
        this.producer = producer;
    }

    /**
     * @return amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets new amount
     *
     * @param amount new value of amount.
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }
}
