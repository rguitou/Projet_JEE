package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.admin.ContractVO;
import bdx.iut.info.amap.web.servlet.model.admin.SubscriberVO;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@Singleton
public class AdminManageServlet extends HttpServlet {
    /**
     * constant for template location
     */
    private static final String BOOTFREE_TEMPLATE = "templates/main_admin.ftl";
    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(AdminManageServlet.class);


    @Inject
    private ContractDao contractDao;

    @Inject
    private SubscriberDao subscriberDao;

    private String message;

    private String message2;


    /**
     * Operate the administration actions depending on the request.
     * Better to have a controller pattern ...
     *
     * @param request
     */
    private List<Contract> manageActions(HttpServletRequest request) {
        List<Contract> contracts = contractDao.findAll();
        String action = request.getParameter("action");
        message = "";
        message2 = ""; //les messages d'erreurs
        if (null == action) return contracts;

        if (action.equals("delContract")){
            Long contractId = Long.parseLong(request.getParameter("contractId"));
            if(subscriberDao.countForContract(contractId) == 0) {
                try {
                    contractDao.delete(contractDao.read(contractId));
                    message = "Contrat effacé";
                }catch(NullPointerException e){
                    message = "Contrat inexistant";
                }
            }else{
                message = "Erreur: ce contrat comporte encore des abonnés";
                request.getParameter("action");
            }
            contracts = contractDao.findAll();
        } else if (action.equals("delSubscriber")) {
            String subscriberId = request.getParameter("subscriberId");
            subscriberDao.delete(subscriberDao.read(subscriberId));
        } else if (action.equals("addContract")) {
            try{
                Double amount = Double.parseDouble(request.getParameter("contractAmount"));
                String name = request.getParameter("contractName");
                String producer = request.getParameter("contractProducer");

                Contract c = new Contract();
                c.setTitle(name);
                c.setProducer(producer);
                c.setAmount(amount);
                contractDao.create(c); // TODO manage collision
            }catch(PersistenceException e) {
                message = "Entrez un nom de contrat différent ou évitez de rafraîchir la page";
            }catch(NumberFormatException e){
                message = "Remplissez tous les champs";
            }catch(ConstraintViolationException e){
                message = "Montant invalide : le montant doit être un multiple de 5 ou un multiple de 5 décrémenté de 1 (4, 9, 14...)";
            }
            contracts = contractDao.findAll();
        } else if (action.equals("addSubscriber")) {
            try {
                String id = request.getParameter("subscriberId");
                String name = request.getParameter("subscriberName");
                String surname = request.getParameter("subscriberSurname");
                String email = request.getParameter("subscriberEmail");
                String address = request.getParameter("subscriberAddress");

                Subscriber s = new Subscriber();
                s.setId(id);
                s.setName(name);
                s.setSurname(surname);
                s.setMail(email);
                s.setAddress(address);
                subscriberDao.create(s); // TODO manage collision
            }catch(RollbackException e){
                message2 = "Remplissez bien tous vos champs dans le format demandé";
            }
        }else if(action.equals("search")){
            String search = request.getParameter("search");
            contracts = contractDao.findByKeyword(search);
        }else if(action.equals("sortByTitle")){
            Collections.sort(contracts, new Comparator<Contract>() { // l'objet Comparator décide de la façon de trier le tableau de contrats
                @Override
                public int compare(Contract contract, Contract t1) {
                    return contract.getTitle().toUpperCase().compareTo(t1.getTitle().toUpperCase()); // par titre
                }
            });
        }else if(action.equals("sortByAmount")){
            Collections.sort(contracts, new Comparator<Contract>() {
                @Override
                public int compare(Contract contract, Contract t1) {
                    double one = contract.getAmount();
                    double two = t1.getAmount();
                    return (int)((int)two - one); //par montant
                }
            });
        }else if(action.equals("sortBySubNumber")){
            Collections.sort(contracts, new Comparator<Contract>() {
                @Override
                public int compare(Contract contract, Contract t1) {
                    long one = subscriberDao.countForContract(contract.getId());
                    long two = subscriberDao.countForContract(t1.getId());
                    return (int) (two - one); // par nombre d'abonnés
                }
            });
        }

        return contracts;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final List<Contract> contracts = manageActions(request);


        final List<ContractVO> contractsVO = new ArrayList<>();
        for (Contract contract : contracts) {
             contractsVO.add(convert(contract));
        }
        final List<Subscriber> subscribers = subscriberDao.findAll();
        final List<SubscriberVO> subscribersVO = new ArrayList<>();
        for (Subscriber subscriber : subscribers) {
            subscribersVO.add(convert(subscriber));
        }

        // Retreive objects to feed the template
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("contracts", contractsVO);
        root.put("subscribers", subscribersVO);
        root.put("message", message);
        root.put("message2", message2);

        // XXX Boilercode for templates to copy paste on all serveltes
        // Manage freemarker stuff
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(BOOTFREE_TEMPLATE, TEMPLATE_ENCODING);
        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);

        }

        // navigation data and links
        root.put("title", "Java EE - AMAP Manager - v0.1 - Romain Giot");


        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e);
        }

        // set mime type
        response.setContentType("text/html");

    }

    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        vo.setNumberOfSubscribers(subscriberDao.countForContract(contract.getId()));
        return vo;
    }

    private SubscriberVO convert(final Subscriber subscriber) {
        SubscriberVO vo = new SubscriberVO();
        vo.setId(subscriber.getId());
        vo.setName(subscriber.getName());
        vo.setAddress(subscriber.getAddress());
        vo.setSurname(subscriber.getSurname());
        vo.setMail(subscriber.getMail());
        vo.setNumberOfSubscriptions(subscriber.numberOfSubscriptions());
        vo.setSubscriptionsAmount(subscriber.subscriptionsAmount());
        return vo;
    }
}
