package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.admin.ContractVO;
import bdx.iut.info.amap.web.servlet.model.admin.SubscriberVO;
import com.google.inject.Inject;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by rguitou on 10/03/18.
 */

@Singleton
public class VisitorServlet extends HttpServlet {

    @Inject
    private ContractDao contractDao;

    @Inject
    private SubscriberDao subscriberDao;

    
    private static final Logger logger = LoggerFactory.getLogger(VisitorServlet.class);



    final List<ContractVO> contractsVO = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        final String action = request.getParameter("action");
        String searchValue = request.getParameter("searchValue");
        List<Contract> contracts = contractDao.findAll();
        if(action != null) {
            if (action.equals("search")) {
                contracts = contractDao.findByKeyword(searchValue);
            } else if (action.equals("sortByTitle")) {
                Collections.sort(contracts, new Comparator<Contract>() {
                    @Override
                    public int compare(Contract contract, Contract t1) {
                        return contract.getTitle().toUpperCase().compareTo(t1.getTitle().toUpperCase());
                    }
                });
            } else if (action.equals("sortByAmount")) {
                Collections.sort(contracts, new Comparator<Contract>() {
                    @Override
                    public int compare(Contract contract, Contract t1) {
                        double one = contract.getAmount();
                        double two = t1.getAmount();
                        return (int) ((int) two - one);
                    }
                });
            } else if (action.equals("sortBySubNumber")) {
                Collections.sort(contracts, new Comparator<Contract>() {
                    @Override
                    public int compare(Contract contract, Contract t1) {
                        long one = subscriberDao.countForContract(contract.getId());
                        long two = subscriberDao.countForContract(t1.getId());
                        return (int) (two - one);
                    }
                });
            }
        }


        for (Contract contract : contracts) {
            contractsVO.add(convert(contract));
        }

        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/visitor.ftl");
        } catch (IOException e) {
            System.out.println("Unable to process request, error during freemarker template retrieval."); }
        Map<String, Object> root = new HashMap<String, Object>();
// navigation data and links
        root.put("title", "Visitor View");
        root.put("contracts", contractsVO);

        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
            contractsVO.clear();}
        catch (TemplateException e) { e.printStackTrace(); }
// set mime type
        response.setContentType("text/html");

    }


    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        vo.setNumberOfSubscribers(subscriberDao.countForContract(contract.getId()));
        return vo;
    }

}
