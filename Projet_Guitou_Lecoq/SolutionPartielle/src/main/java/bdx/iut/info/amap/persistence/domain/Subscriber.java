package bdx.iut.info.amap.persistence.domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rgiot on 06/02/17.
 */
@Entity
public class Subscriber implements Serializable {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(Subscriber.class);

    /**
     * Subscriber identifier (aka login)
     */
    @Id // for unit test where we do not force login
    private String id; // ID == login

    /**
     * Subscriber's name
     */
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^[A-Z-a-z]*$")
    @Column(nullable = false)
    private String name;

    /**
     * Subscriber's surname
     */
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^[A-Z-a-z]*$")
    @Column(nullable = false)
    private String surname;

    /**
     * Subscriber's email
     */
    @NotNull
    @NotEmpty
    @Email
    @Column(nullable = false)
    private String email;

    /**
     * Subscriber's address
     */
    @NotNull
    @Column
    private String address;

    /**
     * Subscribed contracts
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "subscribers_contracts",
            joinColumns = @JoinColumn(name = "subscriber_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "contract_id", referencedColumnName = "ID"))
    private Set<Contract> contracts = new HashSet<>();


    public void subscribe(final Contract c) {
        assert (c != null && c.getId() > 0);
        contracts.add(c);
    }

    public boolean isSubscribedTo(final Contract c) {
        for (Contract c2 : contracts) {
            if (c.getId() == c2.getId())
                return true;
        }
        return false;
    }

    public void unsubscribe(final Contract c) {
        assert (c != null);
        boolean removed = contracts.remove(c);
        logger.debug("Contract with id '{}' was {} for subscriber '{}'", new Object[]{c.getId(), removed ? "removed" : "not removed", id});
    }

    public float subscriptionsAmount() {
        float amount = 0;
        for (Contract c : contracts) {
            amount += c.getAmount();

        }
        return amount;
    }

    public int numberOfSubscriptions() {
        return contracts.size();
    }

    public String toString() {
        return name + " " + surname + " " + address + " " + subscriptionsAmount();
    }

    /**
     * @return contracts
     */
    public Set<Contract> getContracts() {
        return contracts;
    }

    /**
     * Sets new contracts
     *
     * @param contracts new value of contracts.
     */
    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets new id
     *
     * @param id new value of id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return email
     */
    public String getMail() {return email;}

    /**
     * Sets new email
     *
     * @param email new value of email
     */
    public void setMail(String email) {this.email = email; }

    /**
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets new address
     *
     * @param address new value of address.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets new surname
     *
     * @param surname new value of surname.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets new name
     *
     * @param name new value of name.
     */
    public void setName(String name) {
        this.name = name;
    }
}
