package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class AuthSubscriberServlet extends HttpServlet { //page d'authentification d'un abonné

    @Inject
    private SubscriberDao subscriberDao;

    public void manageActions(HttpServletRequest request, HttpServletResponse response){

        String subscriberName = request.getParameter("subscriberName");
        String subscriberId = request.getParameter("subscriberId");

        String action = request.getParameter("action");
        if(null != action) {
            if (action.equals("login")) {
                auth(subscriberName, subscriberId, request, response);
            }
        }
    }

    public void auth(String subscriberName, String subscriberId, HttpServletRequest request ,HttpServletResponse response){
        Subscriber logged = subscriberDao.read(subscriberId);// si l'on trouve l'id de l'abonné
        try {
            if(logged.getName().equals(subscriberName)) // son nom
                response.sendRedirect("http://localhost:"+ request.getLocalPort() + "/amap/subscriber/subsheet?subscriberId="+logged.getId());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response){

        manageActions(request, response);

        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());

        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/auth_sub.ftl");
        }catch (IOException e) {
            System.out.println("Unable to process request, error during freemarker template retrieval.");
        }

        Map<String, Object> root = new HashMap<String, Object>();
        root.put("title", "Login");
        response.setCharacterEncoding("utf-8");


        try {
            PrintWriter out = response.getWriter();

            assert freemarkerTemplate != null;
            freemarkerTemplate.process(root, out);
            out.close();}
        catch (TemplateException e) { e.printStackTrace(); }
        catch ( IOException e ){ e.printStackTrace();}
// set mime type
        response.setContentType("text/html");
    }
}
