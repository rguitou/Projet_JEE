package bdx.iut.info.amap.persistence.domain;


import bdx.iut.info.amap.constraints.IsContract;
import bdx.iut.info.amap.constraints.isAmount;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by rgiot on 02/02/18.
 */
@Entity
public class Contract implements Serializable {

    /**
     * Contract identifier (generated)
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Contract title that match a specific rule {@link IsContract}
     */
    @NotNull
    @IsContract
    @Column(unique = true)
    private String title;

    /**
     * Producer associated to the contract
     */
    @NotNull
    private String producer;

    /**
     * Amount..
     */
    @NotNull
    @isAmount
    private double amount;


    /**
     * @return producer
     */
    public String getProducer() {
        return producer;
    }

    /**
     * Sets new producer
     *
     * @param producer new value of producer.
     */
    public void setProducer(final String producer) {
        this.producer = producer;
    }

    /**
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets new id
     *
     * @param id new value of id.
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets new amount
     *
     * @param amount new value of amount.
     */
    public void setAmount(final double amount) {
        this.amount = amount;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets new title
     *
     * @param title new value of title.
     */
    public void setTitle(final String title) {
        this.title = title;
    }
}
