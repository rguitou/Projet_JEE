package bdx.iut.info.amap.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.lang.Integer.parseInt;

public class isAmountValidator implements ConstraintValidator<isAmount, Double> {

    @Override
    public void initialize(final isAmount isMajor) {

    }

    /**
     * A valid string starts by an uppercase character and ends by a dot
     */
    @Override
    public boolean isValid(final Double value, final ConstraintValidatorContext constraintValidatorContext) {
        return value.intValue() % 5 == 0 || value.intValue() % 5 == 4;
    }
}
