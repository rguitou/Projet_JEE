package bdx.iut.info.amap.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by fred on 10/02/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = isAmountValidator.class)
public @interface isAmount {
    String message() default "Montant de souscription invalide";
    Class<?>[] groups() default {}; // Groupe de validation partiel
    Class<? extends Payload>[] payload() default{}; // Metadonnée de la contrainte
}
