package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.admin.ContractVO;
import bdx.iut.info.amap.web.servlet.model.admin.SubscriberVO;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class ContractManageServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(HomeServlet.class);

    @Inject
    private ContractDao contractDao;

    @Inject
    private SubscriberDao subscriberDao;

    public void manageActions(HttpServletRequest request){
        final long givenContractId = Long.parseLong(request.getParameter("contractId"));
        Contract contract = contractDao.read(givenContractId);
        final String action = request.getParameter("action");

        if (null != action) {
            if (action.equals("modifyContract")) {
                String title = request.getParameter("contractTitle");
                String producer = request.getParameter("contractProducer");
                String amountParser = request.getParameter("contractAmount");
                amountParser.replace(',', '.');
                double amount;
                try {
                    amount = Double.valueOf(amountParser);
                }catch(NumberFormatException e){
                    amount = 1.00;
                    e.printStackTrace();
                }

                contract.setTitle(title);
                contract.setProducer(producer);
                contract.setAmount(amount);

                contractDao.update(contract);
            }else if (action.equals("delSubscriber")){
                Subscriber s = subscriberDao.read(request.getParameter("subscriberId"));
                s.unsubscribe(contract);
                subscriberDao.update(s);
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        manageActions(request);

        final long givenContractId = Long.parseLong(request.getParameter("contractId"));
        Contract contract = contractDao.read(givenContractId);

        final ContractVO contractVO = convert(contract);
        final List<Subscriber> subs = subscriberDao.findForContract(contract.getId());
        final List<SubscriberVO> subsVO = new ArrayList<>();

        for (Subscriber s : subs) {
            final SubscriberVO sVO = convert(s);
            subsVO.add(sVO);
        }

        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());

        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/main_contract.ftl");
        }catch (IOException e) {
            System.out.println("Unable to process request, error during freemarker template retrieval.");
        }



        Map<String, Object> root = new HashMap<String, Object>();
        root.put("title", "Contract management");
        root.put("contract", contractVO);
        root.put("subscribers", subsVO);
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;

        try {
            freemarkerTemplate.process(root, out);
            out.close();}
        catch (TemplateException e) { e.printStackTrace(); }
// set mime type
        response.setContentType("text/html");
    }

    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        return vo;
    }

    private SubscriberVO convert(final Subscriber subscriber) {
        SubscriberVO vo = new SubscriberVO();
        vo.setId(subscriber.getId());
        vo.setName(subscriber.getName());
        vo.setAddress(subscriber.getAddress());
        vo.setMail(subscriber.getMail());
        vo.setSurname(subscriber.getSurname());
        for (Contract contract : subscriber.getContracts()) {
            ContractVO cVO = new ContractVO();
            cVO.setId(contract.getId());
            cVO.setAmount(contract.getAmount());
            cVO.setProducer(contract.getProducer());
            cVO.setTitle(contract.getTitle());
        }
        return vo;
    }

}
