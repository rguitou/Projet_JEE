package bdx.iut.info.amap.web.servlet.model.admin;

/**
 * Defines a subscriber for the admin view layer (Subscriber Visual Object)
 */
public class SubscriberVO {

    /**
     * Subscriber identifier (aka login)
     */
    private String id;

    /**
     * Subscriber's name
     */
    private String name;

    /**
     * Subscriber's surname
     */
    private String surname;

    /**
     * Subscriber's email
     */
    private String email;

    /**
     * Subscriber's address
     */
    private String address;

    /**
     * Number of subscriptions
     */
    private int numberOfSubscriptions;

    /**
     * Subscription amount
     */
    private float subscriptionsAmount;

    /**
     * @return email
     */
    public String getMail() { return email; }

    /**
     * Sets new email
     *
     * @params email new value of email
     */
    public void setMail(String email) { this.email = email; }

    /**
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets new address
     *
     * @param address new value of address.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets new surname
     *
     * @param surname new value of surname.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets new id
     *
     * @param id new value of id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets new name
     *
     * @param name new value of name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return numberOfSubscriptions
     */
    public int getNumberOfSubscriptions() {
        return numberOfSubscriptions;
    }

    /**
     * Sets new numberOfSubscriptions
     *
     * @param numberOfSubscriptions new value of numberOfSubscriptions.
     */
    public void setNumberOfSubscriptions(int numberOfSubscriptions) {
        this.numberOfSubscriptions = numberOfSubscriptions;
    }

    /**
     * @return subscriptionsAmount
     */
    public float getSubscriptionsAmount() {
        return subscriptionsAmount;
    }

    /**
     * Sets new subscriptionsAmount
     *
     * @param subscriptionsAmount new value of subscriptionsAmount.
     */
    public void setSubscriptionsAmount(float subscriptionsAmount) {
        this.subscriptionsAmount = subscriptionsAmount;
    }
}
