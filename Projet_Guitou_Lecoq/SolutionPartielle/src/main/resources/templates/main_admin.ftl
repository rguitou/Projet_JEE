<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="page-header">
    <h1>Java EE - Admin view </h1>
</div>



<div class="panel panel-default">
    <div class="panel-heading">Contracts management</div>
    <div class="panel-body">
        <form>
            <p><span>Contract title</span><input name="contractName" pattern="\[(poisson|fruit|viande|légume|divers)\] Contrat .*"/></p>
            <p><span>Producer</span><input name="contractProducer" /></p>
            <p><span>Amount</span><input name="contractAmount" type="number"/></p>

            <p>
                <input name="action" value="addContract" type="hidden" />
                <input type="submit" value="Add" />
            </p>
        </form>
        <form>
            <input type="text" name="search"/>
            <input name="action" value="search" type="hidden" />
            <input type="submit" value="Search" >
        </form>
        <br>
        <table class="table">
            <tr>
                <th>Title
                    <form>
                        <input  name="action" type="hidden" value="sortByTitle"/>
                        <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Producer</th>
                <th>Amount
                    <form>
                        <input  name="action" type="hidden" value="sortByAmount"/>
                        <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Subscribers
                    <form>
                        <input  name="action" type="hidden" value="sortBySubNumber"/>
                        <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Action</th>
            </tr>

        <#list contracts as contract>
            <tr>
                <td> <a href="../admin/contract?contractId=${contract.getId()}"> ${contract.getTitle()} </a></td>
                <td>${contract.getProducer()}</td>
                <td>${contract.getAmount()}</td>
                <td>${contract.getNumberOfSubscribers()}</td>
                <td>
                    <form>
                        <input name="contractId" value="${contract.getId()}" type="hidden"/>
                        <input name="action" value="delContract"  type="hidden"/>
                        <input type="submit" value="Delete"/>
                    </form>
                </td>
            </tr>
        </#list>
        <p>${message}</p>
    </table>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Subscribers</div>
    <div class="panel-body">
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Address</th>
                <th>Email</th>
                <th>Number of contracts</th>
                <th>Amount of contracts</th>
                <th>Action</th>
            </tr>

            <#list subscribers as subscriber>
            <tr>
                <td><a href="../subscriber/subsheet?subscriberId=${subscriber.getId()}">${subscriber.getName()}</a></td>
                <td>${subscriber.getSurname()}</td>
                <td>${subscriber.getAddress()!}</td>
                <td>${subscriber.getMail()}</td>
                <td>${subscriber.getNumberOfSubscriptions()}</td>
                <td>${subscriber.getSubscriptionsAmount()}</td>
                <td>
                    <form>
                        <input name="subscriberId" value="${subscriber.getId()}" type="hidden"/>
                        <input name="action" value="delSubscriber" type="hidden" />
                        <input type="submit" value="Delete"/>
                    </form>
                </td>
            </tr>
        </#list>
    </table>


    <div>
        <p>${message2}</p>
        <form>
            <p><span>Login</span></p><input required name="subscriberId" />
            <p><span>Name</span></p><input required name="subscriberName" />
            <p><span>Surname</span></p><input required name="subscriberSurname" />
            <p><span>Email</span></p><input required name="subscriberEmail" />
            <p><span>Address</span></p><input name="subscriberAddress" />
            <input name="action" value="addSubscriber" type="hidden" />
            <input type="submit" value="Add" />
        </form>
    </div>
</div>
</div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
        </body>
        </html>