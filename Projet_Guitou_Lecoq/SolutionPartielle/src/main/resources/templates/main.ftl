<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
  <div class="page-header">
    <h1>Java EE - Recettes de cuisine - IHM de test </h1>
  </div>



   <div class="panel panel-default">
      <div class="panel-heading">Liste des ingrédients</div>
      <div class="panel-body">


        <form>
            <p><span>Ingrédient</span><input name="ingredientName" /></p>
            <p><span>Mesure</span><select name="ingredientUnitType"  >
                <option value="0">Poids</option>
                <option value="1">Volume</option>
                <option value="2">Unité</option>
                </select>
            </p>
            <p>
                <input name="action" value="addIngredient" type="hidden" />
                <input type="submit" value="Ajouter" />
            </p>
        </form>

        <table class="table">
            <tr>
                <th>Ingrédient</th>
                <th>Type</th>
                <th>Action</th>
            </tr>

        <#list contracts as contract>
            <tr>
                <td>${contract.getName()} : ${ingDAO.countUsagesInReceipes(contract)}</td>
                <td>${contract.getUnitType()}</td>
                <td>
                    <form>
                        <input name="ingredientId" value="${contract.getId()}" type="hidden"/>
                        <input name="action" value="delIngredient"  type="hidden"/>
                        <input type="submit" value="Supprimer" />
                    </form>
                </td>
            </tr>
        </#list>
        </table>
        </div>
   </div>


   <div class="panel panel-default">
      <div class="panel-heading">Liste des recettes</div>
      <div class="panel-body">


    <table class="table">
        <tr>
            <th>Recette</th>
            <th>Temps de préparation</th>
            <th>Temps de cuisson</th>
            <th>Nombre d'étapes</th>
            <th>Nombre d'ingrédients</th>
            <th>Action</th>
         </tr>

         <#list subscribers as subscriber>
           <tr>
             <td><a href="subscriber?receipeId=${subscriber.getId()}">${subscriber.getTitle()}</a></td>
             <td>${subscriber.getPreparationTime()}</td>
             <td>${subscriber.getCookTime()}</td>
             <td>${subscriber.nbSteps()}</td>
             <td>${subscriber.nbIngredients()}</td>
             <td>
                <form>
                    <input name="receipeId" value="${subscriber.getId()}" type="hidden"/>
                    <input name="action" value="delReceipe" type="hidden" />
                    <input type="submit" value="Supprimer"/>
                </form>
             </td>
           </tr>
         </#list>
         </table>


         <div>
         <form action="subscriber">
           <input name="receipeName" />
           <input name="action" value="createReceipe" type="hidden" />
           <input type="submit" value="Ajouter" />
         </form>
         </div>
      </div>
   </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>