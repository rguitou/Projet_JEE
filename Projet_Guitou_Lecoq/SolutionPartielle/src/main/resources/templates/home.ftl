<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <header class="container-fluid entete">
        <h1 class="titre">Java EE Home</h1>
        <style>.entete{background-color: #50FFF5;} .titre{margin: auto;}</style>
    </header>
    <article>
        <a href="admin/init">Espace Administrateur</a><br/>
        <a href="visitor/init">Espace Visiteur</a><br/>
        <a href="subscriber/init">Espace Abonne</a><br/>
    </article>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>