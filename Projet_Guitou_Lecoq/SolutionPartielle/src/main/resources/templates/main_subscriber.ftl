<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<script>
function sure(){
    if(confirm("Are you sure to unsubscribe ?")){
       var conf = document.getElementById("aborted");
       conf.value = "delContract";
    }
}
</script>

<body>
<div class="page-header">
    <h1>Java EE - Subscriber view </h1>
</div>


<div class="panel panel-default">
    <div class="panel-heading">Subscriber management</div>
    <div class="panel-body">
        <form>
            <p><span>Login</span></p><input name="subscriberId" value="${subscriber.getId()}" readonly="readonly"/>
            <p><span>Name</span></p><input name="subscriberName" value="${subscriber.getName()}"/>
            <p><span>Surname</span></p><input name="subscriberSurname" value="${subscriber.getSurname()}"/>
            <p><span>Email</span></p><input name="subscriberEmail" value="${subscriber.getMail()}"/>
            <p><span>Address</span></p><input name="subscriberAddress" value="${subscriber.getAddress()!}"/>
            <input name="action" value="modifySubscriber" type="hidden"/>
            <input type="submit" value="Modify"/>
        </form>
        <br/>
        <p>Total amount: ${price} </p>
        <p>${message}</p>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Subscriptions</div>
    <div class="panel-body">
        <table class="table">
            <tr>
                <th>Title</th>
                <th>Producer</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
    <#list subscriber.getContracts() as contract>
        <tr>
            <td>${contract.getTitle()}</td>
            <td>${contract.getProducer()}</td>
            <td>${contract.getAmount()}</td>
            <td>
                <form>
                    <input name="contractId" value="${contract.getId()}" type="hidden"/>
                    <input name="subscriberId" value="${subscriber.getId()}" type="hidden"/>
                    <input name="action" value="tmp" id="aborted" type="hidden"/>
                    <input type="submit" value="Delete" onclick="sure()"/>
                </form>
            </td>
        </tr>
    </#list>
        </table>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">Available contracts</div>
    <div class="panel-body">
        <table class="table">
            <tr>
                <th>Title</th>
                <th>Producer</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
        <#list availableContracts as contract>
        <tr>
            <td>${contract.getTitle()}</td>
            <td>${contract.getProducer()}</td>
            <td>${contract.getAmount()}</td>
            <td>
                <form>
                    <input name="contractId" value="${contract.getId()}" type="hidden"/>
                    <input name="subscriberId" value="${subscriber.getId()}" type="hidden"/>
                    <input name="action" value="subscribeContract" type="hidden"/>
                    <input type="submit" value="Subscribe"/>
                </form>
            </td>
        </tr>
        </#list>
    </div>
    <p name="box" id="box"></p>

</body>