<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid page-header">
    <h1>Java EE - Visitor view </h1>
</div>
<style>.page-header{background-color: #ABABD2;}</style>
<div class="container-fluid">
    <h2>Contract list</h2>
    <form>
        <input type="text" name="searchValue" />
        <input type="hidden" name="action" value="search" />
        <input type="submit" value="Search" />
    </form>
    <br/>
    <table class="table">
        <tr>
                <th>Title
                    <form>
                        <input  name="action" type="hidden" value="sortByTitle"/>
                        <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Producer</th>
                <th>Amount
                    <form>
                            <input  name="action" type="hidden" value="sortByAmount"/>
                            <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Subscribers
                    <form>
                       <input  name="action" type="hidden" value="sortBySubNumber"/>
                       <input type="submit" value="↓" >
                    </form>
                </th>
                <th>Action</th>
        </tr>
        <#list contracts as contract>
           <tr>
               <td>${contract.getTitle()}</td>
               <td>${contract.getProducer()}</td>
               <td>${contract.getAmount()}</td>
               <td>${contract.getNumberOfSubscribers()}</td>
           </tr>
        </#list>
    </table>
</div>


