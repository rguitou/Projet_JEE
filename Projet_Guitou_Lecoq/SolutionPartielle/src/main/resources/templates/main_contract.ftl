<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header class="container-fluid entete">
        <h1 class="titre">Contract Management</h1>
        <style>.entete{background-color: #50FFF5;} .titre{margin: auto;}</style>
</header>
<div class="panel panel-default">
    <div class="panel-heading">Contract management</div>
    <div class="panel-body">
        <form>
            <p><span>Id</span></p><input name="contractId" value="${contract.getId()}" readonly="readonly"/>
            <p><span>Title</span></p><input name="contractTitle" value="${contract.getTitle()}"/>
            <p><span>Producer</span></p><input name="contractProducer" value="${contract.getProducer()}"/>
            <p><span>Amount </span></p><input name="contractAmount" value="${contract.getAmount()}"/>
            <input name="action" value="modifyContract" type="hidden"/>
            <input type="submit" value="Modify"/>
        </form>
    </div>
</div>
<div class="panel-heading">Subscribers</div>
<div class="panel panel-default">
<table class="table">
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Mail</th>
            </tr>
    <#list subscribers as subscriber>
        <tr>
            <td>${subscriber.getSurname()}</td>
            <td>${subscriber.getAddress()}</td>
            <td>${subscriber.getMail()}</td>
            <td>
                <form>
                    <input name="subscriberId" value="${subscriber.getId()}" type="hidden"/>
                    <input name="contractId" value="${contract.getId()}" type="hidden"/>
                    <input name="action" value="delSubscriber" type="hidden"/>
                    <input type="submit" value="Delete"/>
                </form>
            </td>
        </tr>
    </#list>
   </table>
    </div>
</body>