<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
  <div class="page-header">
    <h1>Java EE - Recettes de cuisine - IHM de test - Gestion d'une recette</h1>
  </div>

 <form>
  <input name="action" value="modifyDetails" type="hidden" />

  <p><span>Nom : </span><input name="receipeName" value="${subscriber.getTitle()}" /></p>
  <p><span>Temps de préparation : </span><input type="number" name="receipeDurationPreparation" value="${subscriber.getPreparationTime()}"/> minutes</p>
  <p><span>Temps de cuisson: </span><input type="number" name="receipeDurationCooking" value="${subscriber.getCookTime()}" /> minutes</p>

  <input name="receipeId" value="${subscriber.getId()}"  type="hidden"/>
  <input type="submit" value="modifier" />
 </form>

 <h2> List des ingrédients </h2>
 <table class="table">
    <tr>
        <th>Ingredient </th>
        <th>Quantité </th>
        <th>Mesure</th>
        <th>Action </th>
     </tr>

    <#list subscriber.getIngredients() as contract>
            <tr>
                <td>${contract.contract.getName()}</td>
                <td>${contract.getQuantity()}</td>
                <td>${contract.getUnit()}</td>
                <td>
                    <form>
                        <input name="ingredientPos" value="${ingredient_index}" type="hidden"/>
                        <input name="action" value="delIngredient"  type="hidden"/>
                        <input name="receipeId" value="${subscriber.getId()}"  type="hidden"/>
                        <input type="submit" value="Supprimer" />
                    </form>
                </td>
            </tr>
        </#list>

 </table>

 <div>
 <form>
  <p>
    <span>Ingrédient : </span>
    <select name="ingredientId">
    <#list contracts as contract>
      <option value="${contract.getId()}" > ${contract.getName()} </option>
    </#list>
    </select>
  </p>
  <p>
    <span>Quantité : </span>
    <input name="ingredientQuantite" type="number" />
  </p>
  <p>
    <span>Unité : </span>
    <input name="ingredientUnit" /> <!-- TODO replace it with Javascript by the possibilities of the type of contract chosen -->
    </p>
    <input name="action" value="addIngredient" type="hidden" />
    <input name="receipeId" value="${subscriber.getId()}"  type="hidden"/>
    <input type="submit" value="Ajouter" />
    </form>
 </div>


<h2>Liste des instructions</h2>
 <table class="table">
 <tr>
    <th>Instruction</th>
    <th>Action</th>
 </tr>
<#list subscriber.getSteps() as step>
 <tr>
    <td>${step}</td>
    <td>
        <form>
          <input name="stepPos" value="${step_index}" type="hidden" />
          <input name="action" value="delStep" type="hidden" />
           <input name="receipeId" value="${subscriber.getId()}"  type="hidden"/>
          <input type="submit" value="Supprimer" />
        </form>
    </td>
 </tr>
</#list>
 </table>

 <div>
 <form>
  <p><span>Nouvelle step: </span> <textarea name="stepContent" ></textarea>
  </p>
  <input name="action" value="addStep" type="hidden"/>
  <input name="receipeId" value="${subscriber.getId()}"  type="hidden"/>
  <input type="submit" value="Ajouter" />
 </form>
 </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>