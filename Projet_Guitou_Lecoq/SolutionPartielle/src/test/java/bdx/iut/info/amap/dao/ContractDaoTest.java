package bdx.iut.info.amap.dao;

import bdx.iut.info.amap.dao.guice.AbstractDaoTest;
import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ContractDaoTest extends AbstractDaoTest {


    @Test
    public void testCreate() {
        ContractDao dao = this.getContractDao();

        // 1st insertion
        Contract contract = new Contract();
        contract.setTitle("[poisson] Contrat de poisson");
        contract.setProducer("fsdf sf");
        contract.setAmount(14);

        long currentId = contract.getId();
        dao.create(contract);
        assertThat(contract.getId() != currentId);
        currentId = contract.getId();


        // 2nd insertion
        contract = new Contract();
        contract.setTitle("[poisson] Contrat de poisson 2");
        contract.setProducer("fsd fdfd f sf");
        contract.setAmount(105);
        dao.create(contract);

        Assertions.assertThat(dao.findAll()).hasSize(2);

        detach(contract); //Remove entity from current session bag

        // retreival
        Contract found = dao.read(currentId);
        Assertions.assertThat(found).isNotNull();
        Assertions.assertThat(found.getTitle()).isEqualTo("[poisson] Contrat de poisson");
        Assertions.assertThat(found.getAmount()).isEqualTo(14);
    }

    @Test
    public void testUpdate() {
        ContractDao dao = this.getContractDao();
        Assert.assertTrue(dao.findAll().size() == 0);

        Contract contract = new Contract();
        contract.setTitle("[poisson] Contrat de poisson");
        contract.setProducer("fsdf sf");
        contract.setAmount(14);
        dao.create(contract);

        long id = contract.getId();

        contract.setTitle("[poisson] Contrat de poisson 2");
        dao.update(contract);

        detach(contract); //Remove entity from current session bag

        Contract found = dao.read(id);
        Assertions.assertThat(found).isNotNull();
        Assertions.assertThat(found.getTitle()).isEqualTo("[poisson] Contrat de poisson 2");
    }

    @Test
    public void testDelete() {
        ContractDao dao = this.getContractDao();
        Assert.assertTrue(dao.findAll().size() == 0);

        Contract contract = new Contract();
        contract.setTitle("[poisson] Contrat de poissond");
        contract.setProducer("fsdf sf");
        contract.setAmount(14);
        dao.create(contract);

        long id = contract.getId();
        Assert.assertFalse(dao.findAll().size() == 0);


        dao.delete(contract);

        Assert.assertTrue(dao.findAll().size() == 0);
    }


    @Test
    public void testPersistAndFindByName() {
        ContractDao dao = this.getContractDao();

        Contract c1 = new Contract();
        c1.setTitle("[poisson] Contrat de poisson");
        c1.setProducer("Mr Poisson");
        dao.create(c1);

        Contract c2 = new Contract();
        c2.setTitle("[légume] Contrat de truffe");
        c2.setProducer("Mr Truffe");
        dao.create(c2);

        detach(c1, c2);

        Assertions.assertThat(dao.findAll()).hasSize(2);
        Assertions.assertThat(dao.findByKeyword("poisson")).hasSize(1);
        Assertions.assertThat(dao.findByKeyword("caviar")).hasSize(0);
    }


    @Test(expected = Exception.class)
    public void testContractDeletionWithSubscribers() {
        List<Subscriber> subscribers;

        SubscriberDao dao = this.getSubscriberDao();
        ContractDao contractDao = this.getContractDao();

        Subscriber subscriber = new Subscriber();
        subscriber.setId("1");
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");

        Contract c = new Contract();
        c.setTitle("[poisson] Contrat de poisson");
        c.setAmount(25);

        dao.create(subscriber);
        contractDao.create(c);

        String subId = subscriber.getId();
        long contId = c.getId();

        Assert.assertNotNull(subId);

        subscriber.subscribe(c);
        dao.update(subscriber);
        Assert.assertEquals(1, subscriber.numberOfSubscriptions());

        contractDao.delete(c);
    }


    private ContractDao getContractDao() {
        return this.injector.getInstance(ContractDao.class);
    }


    private SubscriberDao getSubscriberDao() {
        return this.injector.getInstance(SubscriberDao.class);
    }

}
